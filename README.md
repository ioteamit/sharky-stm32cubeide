# Sharky-STM32CubeIDE
This is the code repository for examples in "Sharky MKR Programmer's Guide".
The PDF file is available at http://www.midatronics.com

The Sharky MKR board is based on the Sharky module that contain STM32WB55CE SoC from STMicroelectronics.

The STM32WB55CE system on chip is a highly integrated low power radio design with two core in a single SoC. 

One ARM Cortex-M4 32-bit core is dedicated to customer application and one ARM Cortex-M0+ 32-bit microprocessor runs the radio networking stack. 

TestStep00: Classic Blink Example using delay loop

TestStep01: Classic Blink Example using RTC clock at 1 Hz

TestStep02: Blink example governed by USR pushbutton

TestStep03: USB demo: periodical messages and ECHO

TestStep04: BLE_HeartRate demo application with code regenerated from the .ioc file

TestStep05: BLE_p2pserver demo application with code regenerated from the .ioc file 

TestStep06: TestStep05 modified to make LED and pushbutton work correctly

TestStep07: Example of using the ADC: reads voltage from analog pins and battery voltage, internal temperature, VBATT and VREF


